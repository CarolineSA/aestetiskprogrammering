class Frikadelle {
  constructor () {
  this.speed = (random(1.5,3.5)); // choose a random speed of each frikadelle between the given interval
  this.pos = new createVector(random(width-80),0);
  }

  move() { //function for moving behaviours
  this.pos.y+=this.speed; //this.pos.y + this.speed;
  }

  show() {
    image(frik,this.pos.x,this.pos.y,80,80);
  }
}
