let rectSize = {
  w:100,
  h:70
};
let rectPosX; //the x position of the rectangle

let frik;
let min_frikadelle = 1; // minimum amout of frikadeller on the screen
let frikadelle =[];

let score =0, lose = 0; // keep track of score

function preload() {
  frik= loadImage('frikadelle.png');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  rectPosX = windowWidth/2; //start position in the centre of the canvas
}

function draw() {
  background(204,229,255);

  //creating the rectangle
  fill(255);
  rectMode(CENTER);
  rect(rectPosX,windowHeight-35,rectSize.w,rectSize.h);

  // calling functions
  checkFrikadelleNum();
  showFrikadelle();
  checkCatching();
  displayScore();
  checkResult();
}

// make sure that there is always one frikadelle on the screen
function checkFrikadelleNum() {
  if (frikadelle.length < min_frikadelle) {
    frikadelle.push(new Frikadelle());
  }
}

function showFrikadelle() {
  for (let i = 0; i < frikadelle.length; i++) {
    frikadelle[i].show();
    frikadelle[i].move();
  }
}

function checkCatching() {
  //meassure the distance between frikadelle & the rect
  for (let i = 0; i < frikadelle.length; i++){
    let d =
      dist(rectPosX,windowHeight-120+rectSize.h,frikadelle[i].pos.x, frikadelle[i].pos.y);

  if (d < rectSize.h) { //close enough as if catching the frikadelle
        score++;
        frikadelle.splice(i,1);
  }else if (frikadelle[i].pos.y >= windowHeight) { //rect missed the frikadelle
        lose++;
      frikadelle.splice(i,1);
  }
  }
}

function displayScore() {
  fill(0);
  textSize(15);
  text('You have catched '+ score + " frikadelle(r)", 10,20);
  text('You have wasted ' + lose + " frikadelle(r)", 10,40);
  text('PRESS the LEFT & RIGHT key to eat the frikadeller',10,60);
}

function checkResult() {
  if (lose > 2) { // When you drop more than 2 frikadeller (3 frikadeller) you lose the game
    fill(0);
    textSize(26);
    textAlign(CENTER);
    text("GAME OVER... TRY AGAIN", windowWidth/2, windowHeight/2);
    noLoop();
  }
}

function keyPressed() { //the x-position of the rectangle is changing by +/-40 when you use the left and right arrow
  if (keyCode === LEFT_ARROW) {
    rectPosX-=40;
  } else if (keyCode === RIGHT_ARROW) {
    rectPosX+=40;
  }
}
