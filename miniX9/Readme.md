**GROUP WORK**

![Alt Text](https://gitlab.com/CarolineSA/aestetiskprogrammering/-/raw/main/miniX9/Flowchart_idea1.png)
![Alt Text](https://gitlab.com/CarolineSA/aestetiskprogrammering/-/raw/main/miniX9/Flowchart_idea2.png)

For our ninth miniX we had to brainstorm two ideas for our final project and then draw two corresponding flowcharts to visualize the project’s algorithmic processes. We knew from the start that we wanted to make something political, satirical and with a bit of humor, so we brainstormed the task, and ended up with the following two ideas:

**First idea:**

Our first  program is based on surveillance capitalism and algorithms. Individually we have all experienced targeted ads that we really  had no idea why we were presented. On Facebook there is a somewhat hidden option on sponsored ads called “Why am I seeing this?” that lists reasons of why the ad is shown. These reasons are typically gender, location and internet activity based. We thought these were sensible but a bit scary, so we wanted to illustrate that and extravagate the possible reasons with a satirical perspective. 

We imagine the program opens up to a basic mac desktop background with a pop up ad for something societally considered “very bad”( like the satanic church) with a button saying “why am I seeing this?”. If the button is pressed there will be displayed countless reasons for why the specific ad is displayed. These reasons can be sensible aswell as non sensible and just plain stupid. The reasons can be for example like “because you are a white American old man”, and “because you ate a banana on the 7th of may”. The intention is to highlight how algorithms surveil us, while putting a satirical spin on it. 

**Second idea:**

Our second idea is addressing the mink situation in Denmark during the peak of the pandemic. The minks were suddenly infected with covid and in fear that it would spread widely & get out of control Mette Frederiksen decided to terminate all minks in Denmark. It was a quick and drastic decision, since it’s a big industry in Denmark and would affect a lot of people. Many of the farms have been family driven throughout generations, so it was a situation with a lot of emotion. It was also debated a lot because there was doubt if there were legal bases to kill the minks. 

The idea is that a picture of Mette and several minks will float around in the screen and when Mette and a mink touches the mink will disappear and the size of Mette will increase. The “game”/program ends when Mette has consumed all the minks on the screen. 

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

We encountered numerous difficulties when trying to simplify how the code is built. It probably stems from being in an environment (the classroom) where you get used to talking the “language” of programming and just accept things as what they are, rather than what they mean. It was also a bit difficult agreeing on how to exactly phrase what we were trying to formulate, but that also created discussions about the syntaxes and what they meant, which were very insightful. 

**What are the technical challenges facing the two ideas and how are you going to address these?**

Looking at the first idea and flowchart 1 the technical challenges will be how exactly we are going to show the reasons to why the person looking at the program is seeing this advertisement. Are we going to make a long list? Are the reasons slowly appearing? or do you need to scroll to make them appear? Besides the questions of how it should look, there is a question of how to. In the second idea the technical challenges will first of all be how we will make Mette consume the Minks. Second of all we think it will be a challenge to make Mette bigger and bigger as she consumes the mink. Lastly we also think there will be a challenge in how to make it move like we want it to. 


**In which ways are the individual and the group flowcharts you produced useful?**

We found that when trying to simplify the code, we understood it more and thought of further usage in different instances. Also as mentioned earlier, when working in groups, you got to hear how the other members would break it down and thereby maybe get another perspective on how the syntax is used, and how to phrase it. Individually it really helped diving deep into the particular code and thereby carefully examining every line. This means that we thoroughly understand it and can explain it to other “non coders” so they understand it. 


**INDIVIDUAL WORK**

![Alt Text](https://gitlab.com/CarolineSA/aestetiskprogrammering/-/raw/main/miniX9/Flowchart_miniX7.png)

**Description of my own flowchart**

We also had to create a flowchart upon one of our individual miniX'. The goal with my flowchart is to elaborate how I created the code for my miniX7 and indirect give the viewer an impression of what my game is about. At the beginning I though it was difficult to translate my code into a flowchart, exspecially because I needed to find other words for techincal terms e.g. for "Class." I also needed to wrap my mind about what to include and what can be left out? I feel like I ended up with a flowchart that outlines my game satisfactory. 
