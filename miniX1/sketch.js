// variabler for farver1
let r1 = 160;
let r2 = 0;
let gra1 = 192;
let gra2 = 192;
let gra3 = 192;
let h = 255;
let g1 = 215;
let g2 = 181;
let g3 = 10;

function setup() {
  createCanvas(600,600);
  background(224,224,224);
}
function draw() {
  noStroke();
// jultræet
  fill(0,102,0);
  triangle(450,480,150,480,300,100);
// juletræets stamme
  fill(51,25,0);
  rect(275,480,50,70);

// julekugler
  fill(r1,r2,r2);
  circle(375,445,30);
  circle(310,230,30);
  fill(gra1,gra2,gra3);
  circle(210,440,30);
  circle(350,335,30);
  fill(h,h,h);
  circle(290,390,30);
  fill(g1,g2,g3);
  circle(255,305,30);

//julestjerne
  fill(236,221,83);
  triangle(340,125,260,125,300,60);
  fill(236,221,83);
  triangle(260,80,340,80,300,145);
}
// få julekugler til at skifte farve
function mousePressed(){
  background(76,153,0)

// variabler for farver2
  r1= 192
  r2= 192
  gra1= 215
  gra2= 181
  gra3 = 10
  g1= 160
  g2= 0
  g3= 0
  h= 64
}
