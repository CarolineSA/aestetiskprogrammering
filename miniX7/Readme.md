![Alt Text](https://gitlab.com/CarolineSA/aestetiskprogrammering/-/raw/main/miniX7/Screenshot.png)

Link to program: https://carolinesa.gitlab.io/aestetiskprogrammering/miniX7/

Link to code: https://gitlab.com/CarolineSA/aestetiskprogrammering/-/blob/main/miniX7/sketch.js

This week we were given the task to choose one of our previous miniX and make some adjustments/improvements. Since I handed in an miniX6 (game) that wasn't working properly, I thought it was the evident choice. I have only made a few changes, due to the fact that I used a lot of time on making the game work. It was a big relief and satisfaction when I finally succeeded with the that.

**How did I improve/change my program?**

The improvement I have made is the behaviour of the rect. In my miniX6 I made the x-position of the rectangle change by +/-40 every time either the right or left arrow key was pressed. You would have to press the key multiple times in a row to make the rect move back & forth in the canvas, which is not only annoying for the player, but also bad for the keyboard, I suppose. In my miniX7 I created the variable rectSpeed= 10, that I used with the syntax "keyIsDown," so now you just need to hold down the button to slide from side to side of the canvas. It has made the rect move more smooth and quick, which is why I have also raised the number of meatballs on the screen from one to two. I feel like this change has made it to a more a proper game and that it is more fun to play. 

**Further improvements (only suggestions)**

I have several suggestions to what else I could have improved, if only I had had more time. I would have liked to change the rect to a photo of a mouth, because it would look more fun and also make more sense than a rect catching/eating meatballs. I also thought about creating a button that said "Play again," and had the function of resetting the game. If you want to play again in the current game, you have to refresh the browser, which doesn't really align with a real game. I think, a button like that would have made my game more credible & given the player a more authentic game experience. Finally, it would also had been fun to add sound in the game, as some of my fellow students have. I think that sound contributes to a more fun game experience, but also makes it more authentic like the button. 

**What is the relation between aesthetic programming and digital culture?**

As the name indicates "Aesthetic programming" deals with the aesthetics of software. The linkage between aesthetic programming & digital culture is that we in aesthetic programming do not only deal with the technical aspect of programming, but also engages with how software & computer programming affects our culture. We address the cultural & political problems that programming have on the digital culture by thinking critically. Annette Vee is quoted in the book "Aesthetic Programming: A Handbook of Software Studies" written by Winnie Soon & Geoff Cox, which sums up the idea of aesthetic programming very well: _"As she puts it,"Seeing programming in light of the historical, social, and conceptual contexts of literacy helps us to understand computer programming as an important phenomenon of communication, not simply as another new skill or technology.""_ (Soon Winnie & Cox, Geoff, "Getting Started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, p.29)

**References**

[Soon Winnie & Cox, Geoff, "Getting Started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020] 




