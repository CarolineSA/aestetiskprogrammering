function setup() {
  createCanvas(700,700);
  background(226,187,157);
  }

// Regnbue
function draw() {
noStroke();
fill(255,0,0);
arc(350,300,500,480,PI,0, CHORD);
fill(255,128,0);
arc(350,300,450,430,PI,0, CHORD);
fill(249,241,10);
arc(350,300,400,380,PI,0, CHORD);
fill(102,204,0);
arc(350,300,350,330,PI,0, CHORD);
fill(0,128,255,);
arc(350,300,300,280,PI,0, CHORD);
fill(102,0,204);
arc(350,300,250,230,PI,0, CHORD);
fill(225,37,194);
arc(350,300,200,180,PI,0, CHORD);
fill(226,187,157);
arc(350,300,150,130,PI,0, CHORD);

// FAMILIEN (Fire medlemmer)

// Kvinde

noStroke();
fill(102,52,0);
arc(160,510,115,210,PI,0, CHORD);
fill(255,229,204);
circle(160,450,70);
fill(255,102,102);
triangle(210,600,110,600,160,485);
// Øjne
fill(255,255,255);
ellipse(148,445,10,7);
ellipse(172,445,10,7);
fill(0,0,0);
circle(148,445,4.2);
circle(172,445,4.2);
// Mund
fill(204,0,102);
arc(160,465,15,10,0,PI, CHORD);
arc(156.2,465,7.5,5,PI,0, CHORD);
arc(163.8,465,7.5,5,PI,0, CHORD);
// Ben & fødder
rect(140,600,10,60);
rect(170,600,10,60);
ellipse(140,660,20,10);
ellipse(180,660,20,10);
// Arme
fill(255,102,102);
beginShape();
vertex(177,524);
vertex(186,544);
vertex(209,558);
vertex(209,550);
endShape(CLOSE);
circle(209,556,15);

beginShape();
vertex(143,524);
vertex(134,544);
vertex(109,558);
vertex(109,550);
endShape(CLOSE);
circle(109,556,15);


// Mand 1

noStroke();
fill(102,102,0);
rect(360,490,80,110);
rect(395,482,10,8);
fill(255,229,204);
circle(399,447,70);
// Hår
fill(51,25,0);
arc(399,435,68,60,PI,0, CHORD);
fill(255,229,204);
triangle(390,425,395,443,410,440);
triangle(379,428,390,443,400,437);
// Mund
fill(255,255,255);
arc(399,460,15,10,0,PI, CHORD);
// Øjne
fill(255,255,255);
ellipse(387,447,10,7);
ellipse(411,447,10,7);
fill(0,0,0);
circle(387,447,4.2);
circle(411,447,4.2);
// Ben & fødder
fill(0,51,102);
rect(375,600,20,70);
rect(405,600,20,70);
fill(51,25,0);
ellipse(380,670,30,10);
ellipse(420,670,30,10);
// Arme
fill(102,102,0);
beginShape();
vertex(440,491);
vertex(440,524);
vertex(470,588);
vertex(470,573);
endShape(CLOSE);
circle(470,580,15);

beginShape();
vertex(360,491);
vertex(360,524);
vertex(330,588);
vertex(330,573);
endShape(CLOSE);
circle(330,580,15);

// Mand 2

noStroke();
fill(133,204,255);
rect(510,490,80,110);
rect(545,482,10,8);
fill(242,243,193);
arc(549,440,68,70,PI,0, CHORD);
fill(255,229,204);
circle(549,447,70);
// Øjne
fill(255,255,255);
ellipse(537,442,10,7);
ellipse(561,442,10,7);
fill(0,0,0);
circle(537,442,4.2);
circle(561,442,4.2);
// Mund
fill(255,255,255);
arc(549,458,15,10,0,PI, CHORD);
// Ben & fødder
fill(0,0,0);
rect(525,600,20,70);
rect(555,600,20,70);
fill(153,0,0);
ellipse(530,670,30,10);
ellipse(570,670,30,10);
// Arme
fill(133,204,255);
beginShape();
vertex(590,491);
vertex(590,524);
vertex(620,588);
vertex(620,573);
endShape(CLOSE);
circle(620,580,15);

beginShape();
vertex(510,491);
vertex(510,524);
vertex(480,588);
vertex(480,573);
endShape(CLOSE);
circle(480,580,15);

// Barn

fill(0,153,153);
ellipse(270,630,48,60);
circle(280,665,15);
circle(260,665,15);
fill(255,229,204);
circle(270,577,50);
// Øjne
fill(255,255,255);
circle(262,572,8);
circle(278,572,8);
fill(0,0,0);
circle(262,572,5);
circle(278,572,5);
// Mund
fill(255,255,255);
circle(270,587,10);
fill(226,187,157);
fill(204,0,0);
arc(270,586,9.4,8.8,0,PI, CHORD);
fill(133,204,255);
// Arme
fill(0,153,153);
beginShape();
vertex(288,610);
vertex(288,615);
vertex(303,625);
vertex(303,620);
endShape(CLOSE);
circle(303,623,7);

fill(0,153,153);
beginShape();
vertex(252,610);
vertex(252,615);
vertex(237,625);
vertex(237,620);
endShape(CLOSE);
circle(237,623,7);
}
