![Alt Text](https://gitlab.com/CarolineSA/aestetiskprogrammering/-/raw/main/miniX4/Screenshot_1.png) 
![Alt Text](https://gitlab.com/CarolineSA/aestetiskprogrammering/-/raw/main/miniX4/Screenshot_2.png)

Link to program: https://carolinesa.gitlab.io/aestetiskprogrammering/miniX4/

Link to code: https://gitlab.com/CarolineSA/aestetiskprogrammering/-/blob/main/miniX4/sketch.js

**My relationship to data capturing**

For this miniX we were given the assignment of creating a sketch/artwork that we would want to submit to the exhibition of "Capture all" on the "Transmediale" festival Berlin. I found it a bit difficult to come up with something artsy and I guess that is obvious in my miniX too. My starting point for this miniX was my own relationship to data and my willingness of sharing my own. I'm not that suspicious or critical when it comes to sharing my data. For most of the time I just agree to the terms & conditions without reading them, and I think a lot of people do the exact same - Especially when it comes to new updates from the apps & websites that you are already using. And when you think about it, we are not really given any other choice - Either you accept or your access will be denied. For an example Cookies is unavoidable nowadays, because they are on almost every website. I have nothing against personalized advertising. I see that as a benefit for me too, because that way I'm mainly getting commercials of products, that I'm interested in. Of course I'm more cautious if I enter a sketchy website, but my general feelings about sharing that type of data is that I don't feel that my private life is being interfered.

But even though it is the same type of data being captured I do find it creepy & uncomfortable, when I'm getting commercials of something that I have talked to a friend about. Because you can't stop thinking: What else are they capturing from my private conversations? Recently, I was on a ski trip and we were hanging out at this restaurant, where my sister compliments the decoration in the room, which was some colourful fans hanging from the ceiling. A couple a days after the exact one popped up on in my sister's commercials - Like wtf? Do they have eyes too?

I would aslo feel violated if somebody gained access to my photos or were using them. I have watch a few tv-programs about people, whose photos were used to create an account that is used for scaming people. I think it's a big problem, that people can get away with that without any consequences, because it's not considered as identity theft.  

**About my program & code**

In this miniX I have learned to create a button and change the styling of it, which is quite different from styling the regular shapes. You have to declare the button and refer to it by using for e.g. button.style(). I have styled the button size, colour, border and more. In this miniX I have drawn upon knowledge from my last miniX and made a loop for a line of text. My code is really simple this time, but I feel like art is sometimes too. I have called my miniX: "Are you sure?"

My work is a critique of my own naive approach, or other people who have the same approach, on data capturing. I think a lot of people, like me, just press the "Accept" button without giving thought to what they are accepting. My goal with this miniX is to get you to reflect on your own willingness to accept and your skepsis to a website where the only thing shown is a button saying "Accept."
I guess my program is a little unfair, because of course you will click the button to see what happens. You're almost ordered to do it by the program, when there is nothing else to do. Like Winnie Soon writes in "aesthetic programming": "It compels you to press it and "calls for interaction." (s. 99).
Besides that, I don't think any people will consider any "danger" to pressing my button, which there also is absolutely no reason to.

