var stor_viser= 225;
var lille_viser= 225;
var cirkel= 30;

function setup() {
createCanvas(windowWidth,windowHeight);
angleMode(DEGREES);
}

function draw() {
background(255);
drawElements();

// Stor viser
push();
translate(windowWidth/2,windowHeight/2);
stroke(255,0,0);
strokeWeight(8);
rotate(stor_viser);
line(0,0,190,190);
stor_viser= stor_viser +2.5;
pop();

// Lille viser
push();
translate(windowWidth/2,windowHeight/2);
stroke(255,0,0);
strokeWeight(8);
rotate(lille_viser);
line(0,0,135,135);
lille_viser= lille_viser + 0.2;
pop();

function drawElements(){
// Urskiven
noStroke();
fill(204,229,255);
ellipse(windowWidth/2,windowHeight/2,650,650);

// Cirklen i midten som urskiverne er monteret på
fill(255,0,0);
noStroke();
ellipse(windowWidth/2,windowHeight/2,20,20);

// Små cirkler på urskiven
push();
translate(windowWidth/2,windowHeight/2);
for (let i = 0; i < 12; i++) {
rotate(cirkel);
fill(255);
stroke(255,0,0);
strokeWeight(2);
ellipse(150,250,40,40);
}
pop();

}

}
