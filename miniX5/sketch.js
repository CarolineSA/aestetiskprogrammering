var x= 43;
var y= 36;

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);
  angleMode(DEGREES);
  noStroke();
  frameRate(1);

}

function draw() {
  for (let i = 0; i < 15; i++) {
    for (let j = 0; j < 15; j++) {
      arc(100*i,y+150*j,90,70,90,270, CHORD);
      }
    if (i < 6) {
      fill(random(150,255),random(150,255),random(150,255));
    } else {
      fill(random(100,200),random(100,200),random(100,200));
    }
  }

  for (let i = 0; i < 15; i++) {
    for (let j = 0; j < 15; j++) {
      arc(i*100+30,y+90*j,90,70,270,90, CHORD);
    }
    if (x > 6) {
      fill(random(120,180),random(120,180),random(120,180));
    }  else {
    fill(random(100,200),random(100,200),random(100,200));
    }
  }
}
